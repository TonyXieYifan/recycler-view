package edu.sjsu.android.exercise2tonyxie;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import edu.sjsu.android.exercise2tonyxie.databinding.RowLayoutBinding;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>{
    private ArrayList<String> data;

    public MyAdapter(ArrayList<String> data){
        this.data = data;
    }

    @NonNull
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Inflate (= create) a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RowLayoutBinding row = RowLayoutBinding.inflate(inflater);
        return new ViewHolder(row);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // get text from your data at this position
        final String current = data.get(position);
        // Set the footer to display corresponding text
        holder.binding.footer.setText("Footer: " + current);
        // Set onClickListener for header
        holder.binding.header.setOnClickListener(v -> {
            String text =
                    "Position " + holder.getAdapterPosition() + " Clicked";
            holder.binding.header.setText(text);
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected final RowLayoutBinding binding;

        public ViewHolder(RowLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}
